<?php

namespace App\Controller;

use LDAP\Result;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
//“Ruta + controlador= página web”
class PageControlle extends AbstractController
{
    #[Route('/')]
    public function home()
    {
        $response = new JsonResponse();
        $response->setData([
            'success' => true,
            'data' => [
                [
                    'id' => 1,
                    'title' => "hola"
                ], [
                    'id' => 2,
                    'title' => "hola2"
                ]
            ]
        ]);
        return $response;
    }
}
