<?php
require_once      'vendor/autoload.php';
include           "modulos/conexion.php";
include           "vendor/firebase/php-jwt/src/JWT.php";

use             Tuupola\base62;
use             \Firebase\JWT\JWT;

$app            = new  \Slim\Slim();
// Autenticación validando token
$authentication = function () use ($app, $conexion) {
      $params = apache_request_headers();
      if (isset($params['authorization'])) {

            $resultadoPerfil      = mysqli_query($conexion, "SELECT COUNT(*)AS total FROM access_token WHERE  token = '{$params['authorization']}' ");
            $filaElim             = mysqli_fetch_array($resultadoPerfil);
            $contador             = ($filaElim["total"]);
            if ($contador == 1) {
            } else {
                  header("Content-Type: application/json");
                  $result = array("success" => false, "message" => "Token invalido para cliente.");
                  echo json_encode($result, JSON_PRETTY_PRINT);
                  exit;
                  $app->stop();
            }
      } else {
            header("Content-Type: application/json");
            $result = array("success" => false, "message" => "Se requiere Authorization: Token.");
            echo json_encode($result, JSON_PRETTY_PRINT);
            exit;
            $app->stop();
      }
};

//api datos de usuarios;
$app->get("/usuarios", $authentication,  function () use ($conexion, $app) {
      $stmtCom      = mysqli_query($conexion, "SELECT * FROM usuarios  ORDER BY nom_usuario  ASC");
      $datos      = array();
      if ($stmtCom != false) {
            while ($fila       = mysqli_fetch_array($stmtCom, MYSQLI_ASSOC)) {
                  $datos[] = $fila;
            }
            if (!empty($datos)) {
                  $result = array("productos" => $datos, "message" => "Ok", "success" => true);
            } else {
                  $result = array("productos" => $datos, "message" => "No se han encontrado resultados", "success" => false);
            }
      } else {
            $result = array("message" => "Se ha producido un error en webservice.", "success" => false);
      }
      header("Content-Type: application/json");
      echo json_encode($result, JSON_PRETTY_PRINT);
      exit;
});

//api buscar y mostrar productos
$app->post("/productos_tabla_buscar",  function () use ($conexion, $app) {
      $params                 = json_decode($app->request->getBody(), true) ?: $app->request->params();
      $filtro          = $params["filtro"];
      $datos           = ($filtro != "") ? "where title LIKE '%$filtro%' or category LIKE '%$filtro%' or description LIKE '%$filtro%'" : "";
      $stmtCom      = mysqli_query($conexion, "SELECT * FROM productos " . $datos);
      $datos      = array();
      if ($stmtCom != false) {
            while ($fila       = mysqli_fetch_array($stmtCom, MYSQLI_ASSOC)) {
                  $datos[] = $fila;
            }
            if (!empty($datos)) {
                  $result = array("productos" => $datos, "message" => "Ok", "success" => true);
            } else {
                  $result = array("productos" => $datos, "message" => "No se han encontrado resultados", "success" => false);
            }
      } else {
            $result = array("message" => "Se ha producido un error en webservice.", "success" => false);
      }
      header("Content-Type: application/json");
      echo json_encode($result, JSON_PRETTY_PRINT);
      exit;
});

//api se crea registro de carrito;
$app->post("/add_carrito",  function () use ($conexion, $app) {
      $params                 = json_decode($app->request->getBody(), true) ?: $app->request->params();
      $cod_producto  = $params["cod_producto"];
      $cantidad_total           =  $params["cantidad_total"];
      $stmtCom      = mysqli_query($conexion, "INSERT INTO carrito (cod_producto,cantidad) VALUES ('$cod_producto','$cantidad_total')");
      $datos      = array();
      if ($stmtCom != false) {
            $result = array("message" => "Ok", "success" => true);
      } else {
            $result = array("message" => "Se ha producido un error en webservice.", "success" => false);
      }
      header("Content-Type: application/json");
      echo json_encode($result, JSON_PRETTY_PRINT);
      exit;
});

//api listado de los registro de carrito;
$app->get("/lis_carrito",  function () use ($conexion, $app) {

      $stmtCom      = mysqli_query($conexion, "SELECT c.cantidad,p.title,p.price FROM carrito c INNER JOIN productos p ON  c.cod_producto = p.id_producto");
      $datos      = array();
      if ($stmtCom != false) {
            while ($fila       = mysqli_fetch_array($stmtCom, MYSQLI_ASSOC)) {
                  $datos[] = $fila;
            }
            if (!empty($datos)) {
                  $result = array("carrito" => $datos, "message" => "Ok", "success" => true);
            } else {
                  $result = array("carrito" => $datos, "message" => "No se han encontrado resultados", "success" => false);
            }
      } else {
            $result = array("message" => "Se ha producido un error en webservice.", "success" => false);
      }
      header("Content-Type: application/json");
      echo json_encode($result, JSON_PRETTY_PRINT);
      exit;
});
//api listado de los productos;
$app->get("/productos_tabla",  function () use ($conexion, $app) {

      $stmtCom      = mysqli_query($conexion, "SELECT * FROM productos");
      $datos      = array();
      if ($stmtCom != false) {
            while ($fila       = mysqli_fetch_array($stmtCom, MYSQLI_ASSOC)) {
                  $datos[] = $fila;
            }
            if (!empty($datos)) {
                  $result = array("productos" => $datos, "message" => "Ok", "success" => true);
            } else {
                  $result = array("productos" => $datos, "message" => "No se han encontrado resultados", "success" => false);
            }
      } else {
            $result = array("message" => "Se ha producido un error en webservice.", "success" => false);
      }
      header("Content-Type: application/json");
      echo json_encode($result, JSON_PRETTY_PRINT);
      exit;
});
//api valor total de carrito;
$app->get("/total_carrito",  function () use ($conexion, $app) {

      $stmtCom      = mysqli_query($conexion, "SELECT COUNT(*) as total FROM carrito");
      $datos      = array();
      if ($stmtCom != false) {
            while ($fila       = mysqli_fetch_array($stmtCom, MYSQLI_ASSOC)) {
                  $datos[] = $fila;
            }
            if (!empty($datos)) {
                  $result = array("productos" => $datos, "message" => "Ok", "success" => true);
            } else {
                  $result = array("productos" => $datos, "message" => "No se han encontrado resultados", "success" => false);
            }
      } else {
            $result = array("message" => "Se ha producido un error en webservice.", "success" => false);
      }
      header("Content-Type: application/json");
      echo json_encode($result, JSON_PRETTY_PRINT);
      exit;
});
//api login de usuario
$app->post("/login",  function () use ($conexion, $app) {
      $params                 = json_decode($app->request->getBody(), true) ?: $app->request->params();
      $usuario = $params["usuario"];
      $password = $params["password"];
      $stmtCom      = mysqli_query($conexion, "SELECT  cod_usuario , usuario FROM usuarios where usuario =  '{$usuario}' AND password = '{$password}'");
      $datos      = array();
      if ($stmtCom != false) {
            while ($fila       = mysqli_fetch_array($stmtCom, MYSQLI_ASSOC)) {
                  $datos[] = $fila;
            }
            if (!empty($datos)) {
                  $result = array("usuario" => $datos, "message" => "Ok", "success" => true);
            } else {
                  $result = array("usuario" => $datos, "message" => "No se han encontrado resultados", "success" => false);
            }
      } else {
            $result = array("message" => "Se ha producido un error en webservice.", "success" => false);
      }
      header("Content-Type: application/json");
      echo json_encode($result, JSON_PRETTY_PRINT);
      exit;
});
//api validacion del token 
$app->get("/datos_empresa",  function () use ($conexion, $app) {
      $stmtCom      = mysqli_query($conexion, "SELECT *  FROM fotoacces.access_token ");
      $datos      = array();
      if ($stmtCom != false) {
            while ($fila       = mysqli_fetch_array($stmtCom, MYSQLI_ASSOC)) {
                  $datos[] = $fila;
            }
            if (!empty($datos)) {
                  $result = array("datos" => $datos, "message" => "Ok", "success" => true);
            } else {
                  $result = array("datos" => $datos, "message" => "No se han encontrado resultados", "success" => false);
            }
      } else {
            $result = array("message" => "Se ha producido un error en webservice.", "success" => false);
      }
      header("Content-Type: application/json");
      echo json_encode($result, JSON_PRETTY_PRINT);
      exit;
});

//api encargada de traer los datos de los productos
$app->post("/datos_productos",  function () use ($conexion, $app) {
      $params                 = json_decode($app->request->getBody(), true) ?: $app->request->params();
      $cod_producto = $params["cod_producto"];
      $stmtCom      = mysqli_query($conexion, "SELECT * FROM productos where id_producto =  '{$cod_producto}'");
      $datos      = array();
      if ($stmtCom != false) {
            while ($fila       = mysqli_fetch_array($stmtCom, MYSQLI_ASSOC)) {
                  $datos[] = $fila;
            }
            if (!empty($datos)) {
                  $result = array("productos" => $datos, "message" => "Ok", "success" => true);
            } else {
                  $result = array("productos" => $datos, "message" => "No se han encontrado resultados", "success" => false);
            }
      } else {
            $result = array("message" => "Se ha producido un error en webservice.", "success" => false);
      }
      header("Content-Type: application/json");
      echo json_encode($result, JSON_PRETTY_PRINT);
      exit;
});
$app->run();
