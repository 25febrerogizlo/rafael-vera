<?php
include_once("api_modulos.php");
include "./../../modulos/datos.php";
$accion = $_POST['accion'];
switch ($accion) {

        //accion de login
    case 'login':
        $objeto = new \stdClass();
        $usuario = $_POST["usuario"];
        $password = $_POST["password"];
        $objeto->usuario = $usuario;
        $objeto->password = $password;
        $data_obj = json_encode($objeto);
        $resultados = $new->login($enviromment, $apikey, $data_obj);
        $resultados = json_decode($resultados, true);
        if ($resultados["success"] == true) {
            session_start();
            $_SESSION["usuario"] = $resultados["usuario"][0]["usuario"];
            $_SESSION["cod_usuario"] = $resultados["usuario"][0]["cod_usuario"];
        }
        echo json_encode($resultados);
        break;
        //proceso que trae todo los productos indicados 
    case 'sincronizar_producto':
        $objeto = new \stdClass();
        $resultados = $new->sincronizar_producto();
        $resultados = json_decode($resultados, true);
        $datos_tex = "";
        foreach ($resultados as $doc) {
            if ($datos_tex == "") {
                $datos_tex .= "('" . $doc['title'] . "','" . $doc['price'] . "','" . $doc['description'] . "','" . $doc['category'] . "','" . $doc['image'] . "')";
            } else {
                $datos_tex .= ",('" . $doc['title'] . "','" . $doc['price'] . "','" . $doc['description'] . "','" . $doc['category'] . "','" . $doc['image'] . "')";
            }
        }
        echo json_encode($datos_tex);
        break;
        // proceso que se encarga de agregar al carrito
    case 'add_carrito':
        $objeto = new \stdClass();
        $add_carrito = $_POST["cod_producto"];
        $cantidad_total = (!isset($_POST["cantidad_total"])) ? 1 : $_POST["cantidad_total"];
        $objeto->cod_producto = $add_carrito;
        $objeto->cantidad_total = $cantidad_total;
        $data_obj = json_encode($objeto);
        $resultados = $new->add_carrito($enviromment, $data_obj);
        $resultados = json_decode($resultados, true);
        echo json_encode($resultados);
        break;
}
