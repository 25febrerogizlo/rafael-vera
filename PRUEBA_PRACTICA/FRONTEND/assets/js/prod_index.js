function list_productos(){
    const parametros = {
        "filtro": $('#search').val(),
    };
    $.ajax({
        url: "modulos/ajax/productos_ajax.php",
        type: 'POST',
        data: parametros,
        success: function(datos_ajax) {
            // console.log(datos_ajax);
            $("#datos_productos").html(datos_ajax);
        }
    });
}
list_productos();

function add_carrito(id){
    const parametros = {
        "cod_producto": id,
        "accion":"add_carrito"
    };
    $.ajax({
        url: "assets/consultas/consultas.php",
        type: 'POST',
        data: parametros,
        success: function(datos_ajax) {
            const datos_array = JSON.parse(datos_ajax);
            console.log(datos_array.success);
            let valor = parseInt($("#valor_carrito").html()) + 1;
            $("#valor_carrito").html(valor);
        }
    });
}