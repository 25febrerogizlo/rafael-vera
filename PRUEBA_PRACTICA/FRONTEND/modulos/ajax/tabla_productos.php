<?php
include_once("../../assets/consultas/api_modulos.php");
include "../datos.php";

$resultados = $new->productos_tabla($enviromment);
$resultados = json_decode($resultados, true);
$datos_texto = "";
$cont_productos = 0;
if ($resultados["success"] == true) { ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">title</th>
                <th scope="col">price</th>
                <th scope="col">description</th>
                <th scope="col">category</th>
                <th scope="col">image</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($resultados["productos"] as $datosfor) {
                $cont_productos++;
            ?><tr>
                    <td scope="row"><?= $datosfor["id_producto"]  ?></td>
                    <td><?= $datosfor["title"]  ?></td>
                    <td><?= $datosfor["price"]  ?></td>
                    <td><?= $datosfor["description"]  ?></td>
                    <td><?= $datosfor["category"] ?></td>
                    <td><?= $datosfor["image"] ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php
} else {
    $datos_texto .= '<table class="table table-striped">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">SKU</th>
                            <th scope="col">Bodega</th>
                            <th scope="col">ID contifico</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                        </table>
                        <div id="mensaje_error_data" style="min-width: 100%;display: flex;flex-direction: column;align-items: center;">
                        <img src="img/web_20.jpg" style="height: 250px;">
                        <h2>Sin Resultados</h2></tbody>
                        </div>';
}
