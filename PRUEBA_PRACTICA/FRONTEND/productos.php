<?php
session_start();
include_once("assets/consultas/api_modulos.php");
include "modulos/datos.php";
if (isset($_SESSION["usuario"]) && $_SESSION["usuario"] != "" && $_SESSION["usuario"] != null) {
    $nom_usuario = $_SESSION["usuario"];
    $id_usuario = $_SESSION["cod_usuario"];
} else {
    header('Location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="assets/css/productos.css" rel="stylesheet">
    <link href="assets/css/menu_principal.css" rel="stylesheet">
    <link rel=stylesheet href="assets/css/bootstrap.min.css" type="text/css" />
    <link href="assets/fontawesome-free-6.2.1-web/css/fontawesome.css" rel="stylesheet">
    <link href="assets/fontawesome-free-6.2.1-web/css/brands.css" rel="stylesheet">
    <link href="assets/fontawesome-free-6.2.1-web/css/solid.css" rel="stylesheet">
</head>

<body>
    <?php include "pantalla_carga.php"; ?>
    <?php include "modulos/menu_principal.php" ?>
    <div class="contenedor_padre">
        <div class="contenedor_top">
            <div class="text_inicial">
                <h1>Bienvenido al listado de productos</h1>
            </div>
            <div class="contenedor_productos">
                <div class="button_tabla">
                    <button type="button" onclick="listado_tabla()">sincronizar productos</button>
                </div>
                <div id="tabla_producto_res">

                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="./assets/js/jquery/dist/jquery.min.js"></script>
    <script src="./assets/js/js/bootstrap.min.js"></script>
    <script src="./assets/js/ad_productos.js"></script>
</body>

</html>