<?php
include_once("assets/consultas/api_modulos.php");
include "modulos/datos.php";
$objeto = new \stdClass();
$cod_producto = $_GET['cod_prod'];
$objeto->cod_producto = $cod_producto;
$data_obj = json_encode($objeto);
$resultados = $new->datos_productos($enviromment, $data_obj);
$resultados = json_decode($resultados, true);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="assets/css/detalle_producto.css" rel="stylesheet">
</head>

<body>
    <section id="services" class="services section-bg">
        <div class="container-fluid">
            <a href="index.php">
                <button class="boton_volver"> volver</button>
            </a>
            <div class="row row-sm">
                <div class="col-md-6 _boxzoom">
                    <div class="zoom-thumb">
                        <img class="img_pro" src="<?= $resultados['productos'][0]['image']; ?>" alt="">
                    </div>
                    <div class="_product-images">
                        <div class="picZoomer">
                            <img class="my_img" src="<?= $resultados['productos'][0]['image']; ?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="_product-detail-content">
                        <p class="_p-name"> <?= $resultados['productos'][0]['title']; ?> </p>
                        <div class="_p-price-box">
                            <div class="p-list">
                                <span class="price"> $ <?= $resultados['productos'][0]['price']; ?> </span>
                            </div>
                            <div class="_p-add-cart">
                                <div class="_p-qty">
                                    <input type="number" name="qty" id="cantidad_total" value="1" />
                                </div>
                            </div>
                            <div class="_p-features">
                                <span> categoria: </span>
                                <?= $resultados['productos'][0]['category']; ?>
                            </div>
                            <div class="_p-features">
                                <span> Descripción: </span>
                                <?= $resultados['productos'][0]['description']; ?>
                            </div>
                            <form action="" method="post" accept-charset="utf-8">
                                <ul class="spe_ul"></ul>
                                <div class="_p-qty-and-cart">
                                    <div class="_p-add-cart">
                                        <button class="btn-theme btn btn-success" type="button" onclick="add_carrito(<?= $resultados['productos'][0]['id_producto'] ?>)" tabindex="0">
                                            <i class="fa fa-shopping-cart"></i> Agregar al carrito
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="./assets/js/jquery/dist/jquery.min.js"></script>
    <script src="./assets/js/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        function add_carrito(id) {
            const parametros = {
                "cod_producto": id,
                "cantidad_total": $("#cantidad_total").val(),
                "accion": "add_carrito"
            };
            $.ajax({
                url: "assets/consultas/consultas.php",
                type: 'POST',
                data: parametros,
                success: function(datos_ajax) {
                    const datos_array = JSON.parse(datos_ajax);
                    Swal.fire(
                        'Exíto',
                        'Se agrego con exíto al carrito',
                        'success'
                    )
                    console.log(datos_array.success);
                }
            });
        }
    </script>
</body>

</html>