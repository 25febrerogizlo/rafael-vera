<?php
include_once("assets/consultas/api_modulos.php");
include "modulos/datos.php";

$resultados = $new->lis_carrito($enviromment);
$resultados = json_decode($resultados, true);
// var_dump($resultados);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="assets/css/productos.css" rel="stylesheet">
    <link href="assets/css/menu_principal.css" rel="stylesheet">
    <link rel=stylesheet href="assets/css/bootstrap.min.css" type="text/css" />
    <link href="assets/fontawesome-free-6.2.1-web/css/fontawesome.css" rel="stylesheet">
    <link href="assets/fontawesome-free-6.2.1-web/css/brands.css" rel="stylesheet">
    <link href="assets/fontawesome-free-6.2.1-web/css/solid.css" rel="stylesheet">
</head>

<body>
    <?php include "pantalla_carga.php"; ?>
    <div class="contenedor_padre">
        <div class="contenedor_top">
            <div class="text_inicial">
                <h1>Carrito</h1>
            </div>
            <div>
                <table class="table table-striped">
                    <thead>
                        <tr>

                            <th scope="col">Nombre</th>
                            <th scope="col">cantidad</th>
                            <th scope="col">valor</th>
                            <th scope="col">total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($resultados["carrito"] as $datosfor) {
                            $valor_total = $datosfor["cantidad"] * $datosfor["price"];
                        ?><tr>
                                <td><?= $datosfor["title"]  ?></td>
                                <td><?= $datosfor["cantidad"]  ?></td>
                                <td><?= $datosfor["price"]  ?></td>
                                <td><?= $valor_total  ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <button class="btn-success">Comprar ahora</button>
                <a href="index.php">
                    <button class="btn-info">Volver</button>
                </a>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="./assets/js/jquery/dist/jquery.min.js"></script>
    <script src="./assets/js/js/bootstrap.min.js"></script>
    <script src="./assets/js/inicio.js"></script>
</body>

</html>