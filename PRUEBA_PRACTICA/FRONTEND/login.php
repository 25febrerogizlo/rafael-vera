<?php

?>
<html>

<head>
  <meta charset="utf-8" />
  <title>Título de la web</title>
  <link rel=stylesheet href="assets/css/index.css" type="text/css" />
  <link rel=stylesheet href="assets/css/bootstrap.min.css" type="text/css" />
</head>

<body>
  <section class="vh-100">
    <div class="container-fluid h-custom">
      <div class=" d-flex justify-content-center align-items-center h-100">
        <div class="col-md-9 col-lg-6 col-xl-5">
          <img src="img/login.jpg" class="img-fluid" alt="Sample image">
        </div>
        <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
          <form>
            <div class="d-flex titulo_login">
              <p class="lead fw-normal mb-0 me-3">Inicia sesión</p>
            </div>

            <div class="divider d-flex align-items-center my-4">

            </div>

            <!-- Email input -->
            <div class="form-outline mb-4">
              <input type="email" id="user" class="form-control form-control-lg" placeholder="Introducir Usuario" />
              <label class="form-label" for="form3Example3">Usuario</label>
            </div>

            <!-- Password input -->
            <div class="form-outline mb-3">
              <input type="password" id="password" class="form-control form-control-lg" placeholder="Introducir la contraseña" />
              <label class="form-label" for="form3Example4">Contraseña</label>
            </div>

            <div class="d-flex justify-content-between align-items-center">
              <!-- Checkbox -->
              <a href="#!" class="text-body">¿Se te olvidó tu contraseña?</a>
            </div>
            <div class="text-center text-lg-start mt-4 pt-2">
              <button type="button" onclick="login();" class="btn btn-primary btn-lg" style="padding-left: 2.5rem; padding-right: 2.5rem;">Iniciar</button>
              <p class="small fw-bold mt-2 pt-1 mb-0">¿No tienes una cuenta? <a href="#!" class="link-danger">Registro</a></p>
            </div>

          </form>
        </div>
      </div>
    </div>
  </section>
  <div class="d-flex flex-column flex-md-row text-center text-md-start justify-content-between py-4 px-4 px-xl-5 bg-primary">
    <!-- Copyright -->
    <div class="text-white mb-3 mb-md-0">
      FotoAccess © 2022. Todos los derechos reservados.
    </div>
    <!-- Copyright -->

    <!-- Right -->
    <div>
      <a href="#!" class="text-white me-4">
        <i class="fab fa-facebook-f"></i>
      </a>
      <a href="#!" class="text-white me-4">
        <i class="fab fa-twitter"></i>
      </a>
      <a href="#!" class="text-white me-4">
        <i class="fab fa-google"></i>
      </a>
      <a href="#!" class="text-white">
        <i class="fab fa-linkedin-in"></i>
      </a>
    </div>
    <!-- Right -->
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
  <script src="./assets/js/jquery/dist/jquery.min.js"></script>
  <script src="./assets/js/js/bootstrap.min.js"></script>
  <script src="./assets/js/index.js"></script>
</body>

</html>