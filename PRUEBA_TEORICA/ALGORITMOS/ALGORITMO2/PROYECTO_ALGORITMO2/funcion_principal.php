<?php
function generar_codigo()
{
    //variable con los caracteres indicados
    $caracteres_minuscula = 'abcdefghijklmnopqrstuvwxyz';
    $caracteres_mayuscula = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $caracter_especiales = '!@#$%^&*()-_+=~';

    // variables que contienen los valores minimo y maximo que la clave a generar
    $minimo = 8;
    $maximo = 15;

    // variables que indican la validacion del codigo;
    $codigo_final = '';
    $longitud_clave = 0;
    $contado_vueltas = 0;
    // Generar clave aleatoria que cumpla con los requisitos
    while ($contado_vueltas < $minimo || $contado_vueltas > $maximo) {
        $codigo_final = '';
        // se obtiene la longitud del codigo
        $longitud_clave = rand($minimo, $maximo);
        // se divide por la cantidad de caracteres diferente
        $longitud_new = round($longitud_clave / 3);

        $contado_vueltas = 0;
        // Generar clave aleatoria
        for ($i = 0; $i < $longitud_new; $i++) {
            //se ingresa un caracter minuscula
            if ($contado_vueltas < $longitud_clave) {
                $contado_vueltas++;
                $codigo_final .= $caracteres_minuscula[rand(0, strlen($caracteres_minuscula) - 1)];
            }
            //se ingresa un caracter _mayuscula
            if ($contado_vueltas < $longitud_clave) {
                $contado_vueltas++;
                $codigo_final .= $caracteres_mayuscula[rand(0, strlen($caracteres_mayuscula) - 1)];
            }

            //se ingresa un caracter especiales
            if ($contado_vueltas < $longitud_clave) {
                $contado_vueltas++;
                $codigo_final .= $caracter_especiales[rand(0, strlen($caracter_especiales) - 1)];
            }
        }
    }

    return $codigo_final;
}

echo generar_codigo();
