<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .div_contenedor {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            flex-direction: column;
            margin-top: 10%;
            gap: 10px;
        }

        .div_ipunt {
            display: flex;
            align-items: center;
            gap: 10px;
            justify-content: flex-start;
            flex-direction: column;
        }

        .variable_ren {
            color: #1bb0a1;
            font-weight: 600;
        }
    </style>
</head>

<body>
    <div class="div_contenedor">

        <div>
            <button onclick="fincion_codigo()">activar</button>
        </div>
        <div class="div_resultado" id="resultado_cod"></div>
    </div>
    <script src="assets/jquery.min.js"></script>
    <script>
        function fincion_codigo() {
            var xhttp = new XMLHttpRequest();
            // definimos la funcion que resivira el resultado
            xhttp.onreadystatechange = function() {
                console.log(this);
                if (this.readyState == 4 && this.status == 200) {
                    // se ingresara el resultado en el div
                    console.log(this);
                    document.getElementById("resultado_cod").innerHTML = this.responseText;
                }
            };
            xhttp.open("GET", "funcion_principal.php", true);
            xhttp.send();
        }
    </script>
</body>

</html>