<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .div_contenedor {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            flex-direction: column;
            margin-top: 10%;
            gap: 10px;
        }

        .div_ipunt {
            display: flex;
            align-items: center;
            gap: 10px;
            justify-content: flex-start;
            flex-direction: column;
        }

        .variable_ren {
            color: #1bb0a1;
            font-weight: 600;
        }
    </style>
</head>

<body>
    <div class="div_contenedor">
        <div class="div_ipunt">
            <label>Ingresar Texto</label>
            <textarea id="text_inverso"></textarea>
        </div>
        <div class="div_ipunt">
            <label>Ingresar palabra de intercambio</label>
            <input id="text_intercambio">
        </div>
        <div>
            <button onclick="borrar_datos()">Limpiar</button> <button onclick="funcion_invertir()">activar</button>
        </div>
        <div class="div_resultado" id="resultdo_inv"></div>
    </div>
    <script src="assets/jquery.min.js"></script>
    <script>
        function funcion_invertir() {
            $.ajax({
                url: "funcion_principal_ajax.php",
                method: "POST",
                data: {
                    text_inverso: $("#text_inverso").val(),
                    text_intercambio: $("#text_intercambio").val(),
                },
                success: function(resultado_ajax) {
                    $("#resultdo_inv").html(resultado_ajax.resultado);
                }
            });
        }

        function borrar_datos() {
            $("#text_inverso").val("");
            $("#text_intercambio").val("");
            $("#resultdo_inv").val("");
        }
    </script>
</body>

</html>